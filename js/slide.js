$(function(){
	var liwidth = $("#galeria ul li").outerWidth(),
		speed 	=	3500,
		rotate = setInterval(auto, speed);


	//Mostra os botões
	$("section#galeria").hover(function(){
		clearInterval(rotate);
		$("section#buttons").fadeIn();
	},function(){
		$("section#buttons").fadeOut();
		rotate = setInterval(auto, speed);
	});

	//Proximo
	$(".next").click(function(e){
		e.preventDefault();
		$("section#galeria ul").css({'width':'100%'}).animate({left:-liwidth},function(){
			$("#galeria ul li").last().after($("#galeria ul li").first());
			$(this).css({'right':'0','width':'auto'});
			$(this).css({'left':'0','width':'auto'});
		});
		
	});

	//Voltar
	$(".prev").click(function(e){
		e.preventDefault();
		$("#galeria ul li").first().before($("#galeria ul li").last().css({'margin-left':-liwidth}));
		$("section#galeria ul").css({'width':'100%'}).animate({left:liwidth},function(){
			$("#galeria ul li").first().css({'margin-left':'0'});
			$(this).css({'left':'0','width':'auto'});
		});

	});

	//Automatico
	function auto(){
			$(".next").click();
		}
});