function loadCursos() {
  console.log('entrou');
  var saida = '';
  
  var cursos = [];
  
  $.getJSON("data/info.json", function(data) {
      cursos = data.cursos;
      
      for (i = 0; i < cursos.length; i++) {
        saida += '<div class="curso-item">';
        saida += '<div class="curso-item-img">';
        saida += '<img src="' + cursos[i].imagem + '">';
        saida += '</div>';
        saida += '<div class ="curso-item-titulo">'
        saida += '<h3>'+ cursos[i].titulo +'</h3>'
        saida += '</div>'  
        saida += '<div class ="curso-item-desc">'
        saida += '<p>'+ cursos[i].texto +'</p>'
        saida += '</div>'
        saida += '<div class="button">'
        saida += '<a href="#">Ver mais</a>'
        saida += '</div>'  
        saida += '</div>';
      }
      console.log(document.getElementsByClassName('curso-itens')[0]);
      document.getElementsByClassName('curso-itens')[0].innerHTML = saida;
  });
}