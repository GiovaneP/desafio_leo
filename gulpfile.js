"use strict";

const gulp = require("gulp");
const sass = require("gulp-sass");

sass.compiler = require("node-sass"); //necessario para funcionar o gulp

gulp.task('sass',compilaSass);

function compilaSass(){
    return gulp
    .src("css/**.scss")
    .pipe(sass()) //converte sass para css com gulp-sass
    .pipe(gulp.dest("css/"));
}